<?php
/**
 * Eiffel Database
 * Purely JSON
 * Light-Hearted
 *
 * @author Shedrach Okonofua <shedrachokonofua@gmail.com>
 * @copyright 2014
 * @package Eiffel
 * @version Release: Beta
 * 
 * Eiffel to MySql
 * Database - Database
 * Document - Table
 * Entry - Row
*/

class Utils
{
	var $reporting; //Error Reporting

	function createFile($name, $content) //Create a File
	{
	  $handle = fopen($name, "w") or die("Cannot open file: $name"); // Create File
	  file_put_contents($name, $content); // Put Content
	  fclose($handle); //Close File
	}
	function createId($salt) // Create Unique Id
	{ 
	  return rand(111, 999).round(sqrt(time())).md5($salt);
	}
	function errReporting() // Check if error Reporting is on
	{
	  return $this->reporting;
	}
	function error($err) // Reports Error
	{
	  print($err); // Reports Error
	  exit(); // Stops Processing
	}
	function dbExists($name) {
		return is_dir($this->dbStore."/$name/");
	}
	function docExists($name) {
		$db = $this->dbn;
		return is_dir($this->dbStore."/$db/$name");
	}
}
class EntrySearches extends Utils
{
	var $dbStore; //Directory where databases are stored	
	var $db; //Database name
	var $doc; //Document name
	var $result; //Search content for return
	var $count; //Search result count
	var $id;
	
	function __construct($dbs, $db, $doc, $search, $id = false) {
		$this->dbStore = $dbs;
		$this->db = $db;
		$this->doc = $doc;
		$this->result = $search;
		$this->count = count($search);
		$this->id = $id;
	}
	function update($updates) {
  	$db = $this->db;
  	$doc = $this->doc;

    $result = $this->result; // Gets all entries matching search criteria
		if($this->id) {
			foreach($updates as $key => $value) {
				$result[$key] = $value;
			}
		} else {
			foreach($updates as $key => $value) {
				foreach($result as $r) {
					$result[$r][$key] = $value;
				}
			}
		}
		
		if($this->id) {
			$this->createFile($this->dbStore."/$db/$doc/".$this->id.".json", json_encode($result));
		} else {
			foreach($result as $id => $r) {
				$this->createFile($this->dbStore."/$db/$doc/".$id.".json", json_encode($r));
			}
		}
		
		$new = new EntrySearches($this->dbStore, $db, $doc, $result);
		return $new;
  }
  
  function delete() {
  	$db = $this->db;
  	$doc = $this->doc;

    $result = $this->result; // Gets all entries matching search criteria
    foreach($result as $id => $entry) { //Work on individual entries
      unlink($this->dbStore."/$db/$doc/$id.json");
			unset($this->result[$id]);
    }
  }

}
class Document extends Utils
{
	var $dbStore; //Directory where databases are stored	
	var $db; //Database name
	var $doc; //Document name

	function __construct($dbs, $db, $doc) {
		$this->dbStore = $dbs;
		$this->db = $db;
		$this->doc = $doc;
	}
	
	function getCharset($db, $doc)
	{
	  return (array) json_decode(file_get_contents($this->dbStore."/$db/$doc/$doc.json"));
	}
	function docExists($name) {
		$db = $this->db;
		return is_dir($this->dbStore."/$db/$name");
	}
	function enter($data) // Enter a row into a document
	{
		$db = $this->db;
		$doc = $this->doc; 
		if($this->docExists($doc)) {
		    $charset = $this->getCharset($db, $doc); // Get Charset of Document
		    for($i = 0; $i < count($charset); $i++) {//Fill Empty Spaces
		      $data[$i] = isset($data[$i]) ? $data[$i] : "";
		    }
	    
		    $entry = json_encode($data); // Create JSON File Content
		    $fileName = $this->dbStore."/$db/$doc/".$this->createId($entry).".json"; // Name of new file
			$this->createFile($fileName, $entry); // Create Entry
		    $return = new EntrySearches($this->dbStore, $db, $doc, $data);
		}
		else {
		  $this->errReporting() ? $this->error("Document or Database does not exist.") : null; // If Error Reporting is on, report
		  $return = false;
		}
		return $return;
	}
  
	private function filter($key, $value, $matchc) {				
		if(stristr($value, "not(")) {
			$value = str_replace("not(", "", $value);
			$value = str_replace(")", "", $value);
			if($key != $value) {
				$matchc++;
			}
		}
		elseif(stristr($value, "gt(")) {
			$value = str_replace("gt(", "", $value);
			$value = str_replace(")", "", $value);
			if($key > $value) {
				$matchc++;
			}
		}
		elseif(stristr($value, "gtoe(")) {
			$value = str_replace("gtoe(", "", $value);
			$value = str_replace(")", "", $value);
			if($key >= $value) {
				$matchc++;
			}
		}
		elseif(stristr($value, "lt(")) {
			$value = str_replace("lt(", "", $value);
			$value = str_replace(")", "", $value);
			if($key < $value) {
				$matchc++;
			}
		}
		elseif(stristr($value, "ltoe(")) {
			$value = str_replace("ltoe(", "", $value);
			$value = str_replace(")", "", $value);
			if($key <= $value) {
				$matchc++;
			}
		}
		elseif(stristr($value, "has(")) {
			$value = str_replace("has(", "", $value);
			$value = str_replace(")", "", $value);
			if(stristr($key, $value)) {
				$matchc++;
			}
		}
		else {
			if($key == $value) {
				$matchc++;
			}
		}
		return $matchc;
	}
  
  function find($search, $wid = true)
  {
  	$db = $this->db;
  	$doc = $this->doc; 

    $dir = scandir($this->dbStore."/$db/$doc"); // Scan Directory with Entries
    $dir = array_diff($dir, array(".", "..", ".DS_Store", "$doc.json")); // Strip unnecessary values
    $dir = array_values($dir); //Put Array Values in correct order
    $table = array(); // Array for table
    $tablevals = array(); // Array to be used in looping entry contents
    $charset = $this->getCharset($db, $doc); // Get Document Charset
    $entryId = array(); // Array for entry ids
    foreach($dir as $file) { // Get Entries and Ids
      $tablevals[] = (array) json_decode(file_get_contents($this->dbStore."/$db/$doc/".$file)); // Get Entries
      $entryId[] = str_replace(".json", "", $file); // Get Entry Ids
    }
    foreach($tablevals as $tableval) { // Set Entry Values to Charset Keys
      $table[] = array_combine($charset, $tableval);
    }
    $table = array_combine($entryId, $table); // Combine Entries with Ids
    $searhResults = array(); // Array for search results
    foreach($table as $id => $field) {
      $matchc = 0;
      $reqc = count($search);
      foreach($search as $key => $value) {
				if(stristr($value, "&&")){
					$params = explode("&&", $value);
					$reqc = $reqc + count($params) - 1;
					foreach($params as $param) {
						$matchc = $this->filter($field[$key], $param, $matchc);
					}
				}
				elseif(stristr($value, "||")) {
					$params = explode("||", $value);
					foreach($params as $param) {
						if($this->filter($param, $matchc) > $matchc) {
							$matchc = $this->filter($field[$key], $param, $matchc);
							break;
						}
					}
				}
				else {
					$matchc = $this->filter($field[$key], $value, $matchc);
				}
      }
      if($reqc == $matchc) {
        if($wid) {
          $searhResults[$id] = $field;
        }
        else {
          $searhResults[] = $field;
        }
      }
    }
    if(count($search) == 0) { // If Search is empty, return all
      $searhResults = $table;
    }
    $result = array_reverse($searhResults);
    return new EntrySearches($this->dbStore, $db, $doc, $result);
  }
  
  
  function findOne($search)
  {	
  	$db = $this->db;
  	$doc = $this->doc;
		
		$res = false;
		
    $results = $this->find($search)->result; //Perform Search, Get Results
		if(count($results) > 0) {			
			$result = false;
			$id = false;
			foreach($results as $qid => $qresult) {
				$result = $qresult;
				$id = $qid;
			}
			$res = new EntrySearches($this->dbStore, $db, $doc, $result, $id);
		}
    return $res; //Checks Result, If any, returns.
  }
  
  
  
  function findById($id)
  {
  	$db = $this->db;
  	$doc = $this->doc;

    $entry = false; // Declare entry variable, for storing the resultant entry
    $fileLoc = $this->dbStore."/$db/$doc/$id.json"; // Location of entry
    if(file_exists($fileLoc)) { // Checks for entry in db store
      $charset = $this->getCharset($db, $doc); // Gets Charset of entry
      $entry = (array) json_decode(file_get_contents($fileLoc)); // Gets content of entry in array
      $entry = array_combine($charset, $entry); // Associates Charset with entry contents
    }
    else { //If entry with said id is non existent
      $this->reporting ? $this->error("Entry with id : <i>$id</i> doesn't exist.") : $entry = false; //Report if permitted
    }
    return new EntrySearches($this->dbStore, $db, $doc, $entry, $id); // Return Entry
  }
  
  
}

class Eiffel extends Utils {
  var $dbStore; //Directory where databases are stored
  var $dbn; //Database name

  function __construct($dbstore, $reporting = true)
  {
    //declare variables
    $this->dbStore = $dbstore;
    $this->reporting = $reporting;
  }
  
  function setDatabase($name) {
    $this->dbn = $name;
  }
  
  function createDatabase($name) // Create Database 
  {
    $dbLoc = $this->dbStore."/$name"; //Directory of database's json documents
    mkdir($dbLoc); //Create $dbLoc
  }
  function createDoc($name, $charset) //Create Document
  {
    $db = $this->dbn;
    if($this->dbExists($db)) {
      mkdir($this->dbStore."/$db/$name"); // Create Document Folder
      $charsetFileName = $this->dbStore."/$db/$name/$name.json"; // Name of Document Charset File
      $this->createFile($charsetFileName, json_encode($charset)); // Create Charset File
    }
    else {
      $this->errReporting() ? $this->error("Database <i>$db</i> does not exist.") : null; // If Error Reporting is on, report
    }
	return new Document($this->dbStore, $this->dbn, $name);
  }
  function select($doc) {
    return new Document($this->dbStore, $this->dbn, $doc);
  }  
}
